package main

import (
	"gitlab.com/mldlr-labs/url-shortener/internal/app/config"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/router"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/server"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/storage"
	"log"
)

func main() {
	cfg := config.NewConfig()
	repo := storage.New(cfg)
	r := router.NewRouter(repo, cfg)
	s := server.NewServer(r, cfg)
	log.Printf("Starting with cfg: %v", cfg)
	log.Fatal(s.ListenAndServe())
}
