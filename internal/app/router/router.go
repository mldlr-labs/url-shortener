package router

import (
	"github.com/go-chi/chi/v5"
	chiMiddleware "github.com/go-chi/chi/v5/middleware"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/config"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/model"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/router/handlers"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/router/loader"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/router/middleware"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/storage"
	"time"
)

func NewRouter(repo storage.Repository, c *config.Config) chi.Router {

	deleteLoaderCfg := loader.UserLoaderConfig{
		MaxBatch: 200,
		Wait:     5 * time.Second,
		Fetch: func(keys []*model.DeleteURLItem) ([]int, []error) {
			n, err := repo.DeleteURLs(keys)
			if err != nil {
				return []int{n}, []error{err}
			}
			return []int{n}, nil
		},
	}
	deleteLoader := loader.NewUserLoader(deleteLoaderCfg)

	r := chi.NewRouter()
	r.Use(chiMiddleware.Logger)
	r.Use(chiMiddleware.Recoverer)
	r.Use(middleware.Decompress)
	r.Use(middleware.Auth{Config: c}.Authenticate)
	r.Use(chiMiddleware.AllowContentEncoding("gzip"))
	r.Use(chiMiddleware.Compress(5, "application/json", "text/plain"))
	r.Get("/api/user/urls", handlers.APIUserExpand(repo, c))
	r.Post("/api/shorten", handlers.APIShorten(repo, c))
	r.Post("/api/shorten/batch", handlers.APIShortenBatch(repo, c))
	r.Delete("/api/user/urls", handlers.APIDeleteBatch(deleteLoader))
	r.Get("/{id}", handlers.Expand(repo))
	r.Get("/ping", handlers.Ping(repo))
	r.Post("/", handlers.Shorten(repo, c))
	return r
}
