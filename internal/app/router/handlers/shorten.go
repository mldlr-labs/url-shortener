package handlers

import (
	"fmt"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/config"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/model"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/router/middleware"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/storage"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/utils/validators"
	"io"
	"log"
	"net/http"
)

func Shorten(repo storage.Repository, c *config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "error reading request", http.StatusBadRequest)
			return
		}
		defer r.Body.Close()
		long := string(b)
		if !validators.IsURL(long) {
			http.Error(w, "invalid url", http.StatusBadRequest)
			return
		}
		id, err := repo.NewID(long)
		if err != nil {
			http.Error(w, fmt.Sprintf("error getting new id: %v", err), http.StatusInternalServerError)
			return
		}
		userID, found := middleware.GetUserID(r)
		if !found {
			http.Error(w, fmt.Sprintf("error getting user cookie: %v", err), http.StatusInternalServerError)
			return
		}
		url := model.URL{ShortURL: id, LongURL: long, UserID: userID, Deleted: false}
		duplicates, err := repo.Add(r.Context(), &url)
		if err != nil {
			http.Error(w, fmt.Sprintf("error adding record to db: %v", err), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "text/plain")
		if duplicates {
			w.WriteHeader(http.StatusConflict)
		} else {
			w.WriteHeader(http.StatusCreated)
		}
		if _, err = io.WriteString(w, c.BaseURL+"/"+url.ShortURL); err != nil {
			log.Println(err)
		}
	}
}
