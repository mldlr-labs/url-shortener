package handlers

import (
	"encoding/json"
	"fmt"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/config"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/model"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/router/middleware"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/storage"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/utils/validators"
	"net/http"
)

func APIShortenBatch(repo storage.Repository, c *config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var bodyItems []model.BatchReqItem
		var respItems []model.BatchRespItem
		urls := make(map[string]*model.URL, 0)
		if err := json.NewDecoder(r.Body).Decode(&bodyItems); err != nil {
			http.Error(w, "error reading request", http.StatusBadRequest)
			return
		}
		defer r.Body.Close()
		for _, v := range bodyItems {
			if !validators.IsURL(v.OrigURL) {
				respItems = append(respItems, model.BatchRespItem{
					CorID:    v.CorID,
					ShortURL: "incorrect url",
				})
				continue
			}
			id, err := repo.NewID(v.OrigURL)
			if err != nil {
				http.Error(w, fmt.Sprintf("error getting new id: %v", err), http.StatusInternalServerError)
				return
			}
			userID, found := middleware.GetUserID(r)
			if !found {
				http.Error(w, fmt.Sprintf("error getting user cookie: %v", err), http.StatusInternalServerError)
				return
			}
			urls[v.CorID] = &model.URL{
				ShortURL: id,
				LongURL:  v.OrigURL,
				UserID:   userID,
			}
		}
		duplicates, err := repo.AddBatch(r.Context(), urls)
		if err != nil {
			http.Error(w, fmt.Sprintf("error adding record to db: %v", err), http.StatusInternalServerError)
			return
		}
		for _, v := range bodyItems {
			respItems = append(respItems, model.BatchRespItem{
				CorID:    v.CorID,
				ShortURL: fmt.Sprintf("%s/%s", c.BaseURL, urls[v.CorID].ShortURL),
			})
		}
		w.Header().Set("Content-Type", "application/json")
		if duplicates {
			w.WriteHeader(http.StatusConflict)
		} else {
			w.WriteHeader(http.StatusCreated)
		}
		if err := json.NewEncoder(w).Encode(respItems); err != nil || len(respItems) == 0 {
			http.Error(w, "error building the response", http.StatusInternalServerError)
			return
		}
	}
}
