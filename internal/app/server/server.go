package server

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"gitlab.com/mldlr-labs/url-shortener/internal/app/config"
	"net/http"
)

func NewServer(r chi.Router, c *config.Config) *http.Server {
	return &http.Server{
		Handler: r,
		Addr:    fmt.Sprintf(c.ServerAddress),
	}
}
